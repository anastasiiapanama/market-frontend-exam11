import React from 'react';
import {Switch, Route} from "react-router-dom";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppToolbar from "./components/UI/AppToolbar/AppToolbar";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";

import Container from "@material-ui/core/Container";
import NewPost from "./containers/NewPost/NewPost";
import Products from "./containers/Products/Products";
import Product from "./components/Product/Product";
import ProductsList from "./containers/ProductsList/ProductsList";

const App = () => (
    <>
        <CssBaseline/>
        <header>
            <AppToolbar/>
        </header>
        <main>
            <Container maxWidth="xl">
                <Switch>
                    <Route path="/" exact component={Products}/>
                    <Route path="/new-post" exact component={NewPost}/>
                    <Route path="/products/:id" exact component={Product}/>
                    <Route path="/products" exact component={ProductsList}/>
                    <Route path="/register" component={Register}/>
                    <Route path="/login" component={Login}/>
                </Switch>
            </Container>
        </main>
    </>
);

export default App;
