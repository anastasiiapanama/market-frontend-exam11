import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {deleteProduct, fetchSingleProduct} from "../../store/actions/productsActions";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import {Divider} from "@material-ui/core";
import Button from "@material-ui/core/Button";

const Product = ({match}) => {
    const dispatch = useDispatch();
    const product = useSelector(state => state.products.singleProduct);

    useEffect(() => {
        dispatch(fetchSingleProduct(match.params.id));
    }, [dispatch, match.params.id]);

    return product ? (
        <Grid container direction="column" spacing="2">
            <Grid item xs>
                <Typography variant="h5">{product.title}</Typography>
            </Grid>
            {product.image && (
                <Grid item xs>
                    <img src={'http://localhost:8000/' + product.image}
                         alt={product.title} style={{maxWidth: 200, maxHeight: 200}}
                    />
                </Grid>
            )}
            <Grid item xs>
                <Typography variant="body1">Price: {product.price}</Typography>
            </Grid>
            <Grid item xs>
                <Typography variant="body1">Description: {product.description}</Typography>
            </Grid>
            <Grid item xs>
                <Divider/>
            </Grid>
            <Grid item xs>
                <Typography variant="body2">Category: {product.category.title}</Typography>
            </Grid>
            <Grid item xs>
                <Typography variant="body2">Created: {product.user.displayName}</Typography>
            </Grid>
            <Grid item xs>
                <Divider/>
            </Grid>
            <Button onClick={() => dispatch(deleteProduct(product._id))}>Delete comment</Button>
        </Grid>
    ) : null;
};

export default Product;