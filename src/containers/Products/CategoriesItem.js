import React from 'react';
import {Grid, List, ListItem, ListItemIcon, ListItemText} from "@material-ui/core";
import SendIcon from '@material-ui/icons/Send';
import makeStyles from "@material-ui/core/styles/makeStyles";
import {Link} from "react-router-dom";

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
    nested: {
        paddingLeft: theme.spacing(4),
    },
}));

const CategoriesItem = ({title, id}) => {
    const classes = useStyles();

    return (
        <Grid item>
            <List component="nav" className={classes.root}>
                <ListItem button component={Link} to={'/products?category=' + id}>
                    <ListItemIcon>
                        <SendIcon/>
                    </ListItemIcon>
                    <ListItemText primary={title}/>
                </ListItem>
            </List>
        </Grid>
    );
};

export default CategoriesItem;