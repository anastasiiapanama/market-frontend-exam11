import React from 'react';
import PropTypes from 'prop-types';

import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import IconButton from "@material-ui/core/IconButton";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import {Link} from "react-router-dom";
import {makeStyles} from "@material-ui/core/styles";
import {CardMedia} from "@material-ui/core";
import PhotoIcon from '@material-ui/icons/Photo';

import {apiURL} from "../../config";

const useStyles = makeStyles({
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column'
    },
    media: {
        height: 0,
        paddingTop: '56.25%',
    },
    cover: {
        width: '200px',
        height: '200px',
        overflow: 'hidden',
        borderRadius: '4px',
        alignSelf: 'center'
    }
});

const ProductItem = ({title, price, image, id}) => {
    const classes = useStyles();

    if (image) {
        image = <CardMedia
            className={classes.cover}
            image={apiURL + '/' + image}
            title={title}
        />
    } else {
        image = <IconButton>
            <PhotoIcon color="primary" style={{fontSize: 70}}/>
        </IconButton>
    }

    return (
        <Card className={classes.card}>
            <CardHeader title={title}/>
            {image}
            <CardContent>
                <strong style={{marginLeft: '10px'}}>
                    Price: {price} KGS
                </strong>
            </CardContent>
            <CardActions>
                <IconButton component={Link} to={'/products/' + id}>
                    <ArrowForwardIcon/>
                </IconButton>
            </CardActions>
        </Card>
    );
};

ProductItem.propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    image: PropTypes.string
};

export default ProductItem;