import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import {fetchProducts} from "../../store/actions/productsActions";
import ProductItem from "./ProductItem";
import CircularProgress from "@material-ui/core/CircularProgress";
import makeStyles from "@material-ui/core/styles/makeStyles";
import CategoriesItem from "./CategoriesItem";
import {fetchCategories} from "../../store/actions/categoriesActions";

const useStyles = makeStyles(theme => ({
    progress: {
        height: 200
    }
}));

const Products = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const categories = useSelector(state => state.categories.categories);
    const products = useSelector(state => state.products.products);
    const loading = useSelector(state => state.products.productsLoading);

    useEffect(() => {
        dispatch(fetchCategories());
    });

    useEffect(() => {
        dispatch(fetchProducts());
    }, [dispatch]);

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justify="space-between" alignItems="center">
                <Grid item>
                    <Typography variant="h4">All Products</Typography>
                </Grid>
            </Grid>
            <Grid item={3}>
                {categories.map(category => (
                    <CategoriesItem
                        key={category._id}
                        id={category._id}
                        title={category.title}
                    />
                ))}
            </Grid>
            <Grid item container spacing={1}>
                {loading ? (
                    <Grid container justify="center" alignItems="center" className={classes.progress}>
                        <Grid item>
                            <CircularProgress/>
                        </Grid>
                    </Grid>
                ) : products.map(product => (
                    <Grid item xs={12} sm={12} md={6} lg={4} key={product._id}>
                        <ProductItem
                            id={product._id}
                            title={product.title}
                            price={product.price}
                            image={product.image}
                            description={product.description}
                        />
                    </Grid>
                ))}
            </Grid>
        </Grid>
    );
};

export default Products;