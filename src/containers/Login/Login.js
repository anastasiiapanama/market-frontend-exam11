import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Link as RouterLink} from "react-router-dom";
import FormElement from "../../components/UI/Form/FormElement";

import {Avatar, CssBaseline, Link, makeStyles, Paper} from "@material-ui/core";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import {loginUser} from "../../store/actions/usersActions";
import {Alert, AlertTitle} from "@material-ui/lab";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles((theme) => ({
    root: {
        height: '100vh',
    },
    image: {
        backgroundImage: 'url(https://source.unsplash.com/random)',
        backgroundRepeat: 'no-repeat',
        backgroundColor:
            theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    },
    paper: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(2),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

const Login = () => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const [user, setUser] = useState({
        username: '',
        password: ''
    });

    const error = useSelector(state => state.users.loginError);
    const loading = useSelector(state => state.users.loginLoading);

    const inputChangeHandler = e => {
        const {name, value} = e.target;

        setUser(prev => ({
            ...prev,
            [name]: value
        }))
    };

    const submitFormHandler = e => {
        e.preventDefault();
        dispatch(loginUser({...user}));
    };

    return (
        <Grid container component="main" className={classes.root}>
            <CssBaseline />
            <Grid item xs={false} sm={4} md={7} className={classes.image} />
            <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Sign in
                    </Typography>
                    <form className={classes.form} onSubmit={submitFormHandler}>
                        {error && (
                            <Grid item xs>
                                <Alert severity="error">
                                    <AlertTitle>Error</AlertTitle>
                                    {error.message || error.global}
                                </Alert>
                            </Grid>
                        )}
                        <FormElement
                            label="Username"
                            onChange={inputChangeHandler}
                            name="username"
                            type="text"
                            value={user.username}
                        />
                        <FormElement
                            label="Password"
                            onChange={inputChangeHandler}
                            name="password"
                            type="password"
                            value={user.password}
                        />
                        <Grid item xs>
                            <ButtonWithProgress
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                                loading={loading}
                                disabled={loading}
                            >
                                Sign in
                            </ButtonWithProgress>
                        </Grid>
                        <Grid item container justify="flex-end">
                            <Grid item>
                                <Link component={RouterLink} variant="body2" to="/register">
                                    Or sign up
                                </Link>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </Grid>
        </Grid>
    );
};

export default Login;