import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchCategories} from "../../store/actions/categoriesActions";
import {createProduct} from "../../store/actions/productsActions";
import PostForm from "../../components/PostForm/PostForm";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

const NewPost = ({history}) => {
    const dispatch = useDispatch();
    const categories = useSelector(state => state.categories.categories);

    useEffect(() => {
        dispatch(fetchCategories());
    });

    const onPostFormSubmit = async postData => {
        await dispatch(createProduct(postData));

        history.push('/');
    };

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item xs>
                <Typography variant="h4">Create new item</Typography>
            </Grid>
            <Grid item xs>
                <PostForm
                    onSubmit={onPostFormSubmit}
                    categories={categories}
                />
            </Grid>
        </Grid>
    );
};

export default NewPost;