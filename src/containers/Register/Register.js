import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Link as RouterLink} from 'react-router-dom';
import {registerUser} from "../../store/actions/usersActions";

import {Avatar, CssBaseline, Link, makeStyles, Paper} from "@material-ui/core";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import FormElement from "../../components/UI/Form/FormElement";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {Alert, AlertTitle} from "@material-ui/lab";

const useStyles = makeStyles((theme) => ({
    root: {
        height: '100vh',
    },
    image: {
        backgroundImage: 'url(https://source.unsplash.com/random)',
        backgroundRepeat: 'no-repeat',
        backgroundColor:
            theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    },
    paper: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(2),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

const Register = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [user, setUser] = useState({
        username: '',
        password: '',
        displayName: '',
        phone: ''
    });

    const error = useSelector(state => state.users.registerError);
    const loading = useSelector(state => state.users.registerLoading);

    const inputChangeHandler = e => {
      const {name, value} = e.target;

      setUser(prev => ({
          ...prev,
          [name]: value
      }))
    };

    const submitFormHandler = e => {
        e.preventDefault();
        dispatch(registerUser({...user}));
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <Grid container component="main" className={classes.root}>
            <CssBaseline />
            <Grid item xs={false} sm={4} md={7} className={classes.image} />
            <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Sign up
                    </Typography>
                    <form className={classes.form} onSubmit={submitFormHandler}>
                        {error && (
                            <Grid item xs>
                                <Alert severity="error">
                                    <AlertTitle>Error</AlertTitle>
                                    {error.message || error.global}
                                </Alert>
                            </Grid>
                        )}
                        <FormElement
                            label="Username"
                            onChange={inputChangeHandler}
                            name="username"
                            type="text"
                            value={user.username}
                            error={getFieldError('username')}
                        />
                        <FormElement
                            label="Password"
                            onChange={inputChangeHandler}
                            name="password"
                            type="password"
                            value={user.password}
                            error={getFieldError('password')}
                        />
                        <FormElement
                            label="Your name"
                            onChange={inputChangeHandler}
                            name="displayName"
                            type="displayName"
                            value={user.displayName}
                            error={getFieldError('displayName')}
                        />
                        <FormElement
                            label="Phone"
                            onChange={inputChangeHandler}
                            name="phone"
                            type="phone"
                            value={user.phone}
                            error={getFieldError('phone')}
                        />
                        <Grid item xs>
                            <ButtonWithProgress
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                                loading={loading}
                                disabled={loading}
                            >
                                Sign up
                            </ButtonWithProgress>
                        </Grid>
                        <Grid item container justify="flex-end">
                            <Grid item>
                                <Link component={RouterLink} variant="body2" to="/login">
                                    Or log in
                                </Link>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </Grid>
        </Grid>
    );
};

export default Register;