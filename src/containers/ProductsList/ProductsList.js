import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchProductsList} from "../../store/actions/productsActions";
import ProductItem from "../Products/ProductItem";

import Grid from "@material-ui/core/Grid";
import {CircularProgress} from "@material-ui/core";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles({
    card: {
        height: '90%'
    },
    header: {
        padding: 0
    },
    media: {
        height: '54px'
    }
});

const ProductsList = ({match}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const products = useSelector(state => state.products.productsList);
    console.log(products)
    const loading = useSelector(state => state.products.productsLoading);

    useEffect(() => {
        dispatch(fetchProductsList(match.params.id));
    }, [dispatch, match.params.id]);

    return (
        <Grid item container spacing={1}>
            {loading ? (
                <Grid container justify="center" alignItems="center" className={classes.progress}>
                    <Grid item>
                        <CircularProgress/>
                    </Grid>
                </Grid>
            ) : products.map(product => (
                <Grid item xs={12} sm={12} md={6} lg={4}>
                    <ProductItem
                        key={product._id}
                        id={product._id}
                        title={product.title}
                        price={product.price}
                        image={product.image}
                        description={product.description}
                    />
                </Grid>
            ))}
        </Grid>
    );
};

export default ProductsList;