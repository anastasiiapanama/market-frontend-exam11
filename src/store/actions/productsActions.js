import axiosApi from "../../axios-api";
import {NotificationManager} from "react-notifications";
import {historyPush} from "./hystoryActions";

export const FETCH_PRODUCTS_REQUEST = 'FETCH_PRODUCTS_REQUEST';
export const FETCH_PRODUCTS_SUCCESS = 'FETCH_PRODUCTS_SUCCESS';
export const FETCH_PRODUCTS_FAILURE = 'FETCH_PRODUCTS_FAILURE';

export const FETCH_PRODUCTS_LIST_REQUEST = 'FETCH_PRODUCTS_LIST_REQUEST';
export const FETCH_PRODUCTS_LIST_SUCCESS = 'FETCH_PRODUCTS_LIST_SUCCESS';
export const FETCH_PRODUCTS_LIST_FAILURE = 'FETCH_PRODUCTS_LIST_FAILURE';

export const FETCH_PRODUCT_REQUEST = 'FETCH_PRODUCT_REQUEST';
export const FETCH_PRODUCT_SUCCESS = 'FETCH_PRODUCT_SUCCESS';
export const FETCH_PRODUCT_FAILURE = 'FETCH_PRODUCT_FAILURE';

export const CREATE_PRODUCT_SUCCESS = 'CREATE_PRODUCT_SUCCESS';

export const DELETE_PRODUCT_SUCCESS = 'DELETE_PRODUCT_SUCCESS';

export const fetchProductsRequest = () => ({type: FETCH_PRODUCTS_REQUEST});
export const fetchProductsSuccess = products => ({type: FETCH_PRODUCTS_SUCCESS, products});
export const fetchProductsFailure = () => ({type: FETCH_PRODUCTS_FAILURE});

export const fetchProductsListRequest = () => ({type: FETCH_PRODUCTS_LIST_REQUEST});
export const fetchProductsListSuccess = products => ({type: FETCH_PRODUCTS_LIST_SUCCESS, products});
export const fetchProductsListFailure = () => ({type: FETCH_PRODUCTS_LIST_FAILURE});

export const fetchProductRequest = () => ({type: FETCH_PRODUCT_REQUEST});
export const fetchProductSuccess = product => ({type: FETCH_PRODUCT_SUCCESS, product});
export const fetchProductFailure = () => ({type: FETCH_PRODUCT_FAILURE});

export const createProductSuccess = () => ({type: CREATE_PRODUCT_SUCCESS});

export const deleteProductSuccess = id => ({type: DELETE_PRODUCT_SUCCESS, id});

export const fetchProducts = () => {
    return async dispatch => {
        try {
            dispatch(fetchProductsRequest());
            const response = await axiosApi.get('/products');
            dispatch(fetchProductsSuccess(response.data));
            NotificationManager.success('SUCCESS fetch products');
        } catch (e){
            dispatch(fetchProductsFailure());
            NotificationManager.error('Could not fetch products');
        }
    };
};

export const fetchProductsList = id => {
    return async dispatch => {
        try {
            dispatch(fetchProductsListRequest());
            const response = await axiosApi.get('/products?category=' + id);
            console.log(response)
            dispatch(fetchProductsListSuccess(response.data));
            NotificationManager.success('SUCCESS fetch products list');
        } catch (e){
            dispatch(fetchProductsListFailure());
            NotificationManager.error('Could not fetch products');
        }
    };
};

export const fetchSingleProduct = id => {
    return async dispatch => {
        try {
            dispatch(fetchProductRequest());
            const response = await axiosApi.get('/products/' + id);
            NotificationManager.success('SUCCESS fetch product');
            dispatch(fetchProductSuccess(response.data));
        } catch (e) {
            dispatch(fetchProductFailure());
            NotificationManager.error('Could not fetch product');
        }
    };
};

export const createProduct = productData => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {Authorization: token};

            await axiosApi.post('/products', productData, {headers});
            dispatch(createProductSuccess());
            dispatch(historyPush('/'));
            NotificationManager.success('SUCCESS create product');
        } catch (e) {
            NotificationManager.error('Could not create product');
        }
    };
};

export const deleteProduct = id => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {Authorization: token};

            await axiosApi.delete('/products/' + id, {headers});
            dispatch(deleteProductSuccess(id));
            dispatch(historyPush('/'));
            NotificationManager.success('SUCCESS delete product');
        } catch (e) {
            NotificationManager.error('Could not delete product');
        }
    };
};