import {
    DELETE_PRODUCT_SUCCESS,
    FETCH_PRODUCT_FAILURE,
    FETCH_PRODUCT_REQUEST, FETCH_PRODUCT_SUCCESS,
    FETCH_PRODUCTS_FAILURE, FETCH_PRODUCTS_LIST_FAILURE, FETCH_PRODUCTS_LIST_REQUEST, FETCH_PRODUCTS_LIST_SUCCESS,
    FETCH_PRODUCTS_REQUEST,
    FETCH_PRODUCTS_SUCCESS
} from "../actions/productsActions";

const initialState = {
    products: [],
    productsList: [],
    singleProduct: {},
    productsLoading: false,
};

const productsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PRODUCTS_REQUEST:
            return {...state, productsLoading: true};
        case FETCH_PRODUCTS_SUCCESS:
            return {...state, productsLoading: false, products: action.products};
        case FETCH_PRODUCTS_FAILURE:
            return {...state, productsLoading: false};
        case FETCH_PRODUCTS_LIST_REQUEST:
            return {...state, productsLoading: true};
        case FETCH_PRODUCTS_LIST_SUCCESS:
            return {...state, productsLoading: false, productsList: action.products};
        case FETCH_PRODUCTS_LIST_FAILURE:
            return {...state, productsLoading: false};
        case FETCH_PRODUCT_REQUEST:
            return {...state, productsLoading: true};
        case FETCH_PRODUCT_SUCCESS:
            return {...state, productsLoading: false, singleProduct: action.product};
        case FETCH_PRODUCT_FAILURE:
            return {...state, productsLoading: false};
        case DELETE_PRODUCT_SUCCESS:
            return {
                ...state,
                products: state.products.filter(c => c.id !== action.id)
            };
        default:
            return state;
    }
};

export default productsReducer;